// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.6.1
// source: pkg/mensajero.proto

package pkg

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// MensajeroClient is the client API for Mensajero service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MensajeroClient interface {
	Conectar(ctx context.Context, in *Registracion, opts ...grpc.CallOption) (*TokenAutenticacion, error)
}

type mensajeroClient struct {
	cc grpc.ClientConnInterface
}

func NewMensajeroClient(cc grpc.ClientConnInterface) MensajeroClient {
	return &mensajeroClient{cc}
}

func (c *mensajeroClient) Conectar(ctx context.Context, in *Registracion, opts ...grpc.CallOption) (*TokenAutenticacion, error) {
	out := new(TokenAutenticacion)
	err := c.cc.Invoke(ctx, "/mensajero.Mensajero/Conectar", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MensajeroServer is the server API for Mensajero service.
// All implementations must embed UnimplementedMensajeroServer
// for forward compatibility
type MensajeroServer interface {
	Conectar(context.Context, *Registracion) (*TokenAutenticacion, error)
	mustEmbedUnimplementedMensajeroServer()
}

// UnimplementedMensajeroServer must be embedded to have forward compatible implementations.
type UnimplementedMensajeroServer struct {
}

func (UnimplementedMensajeroServer) Conectar(context.Context, *Registracion) (*TokenAutenticacion, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Conectar not implemented")
}
func (UnimplementedMensajeroServer) mustEmbedUnimplementedMensajeroServer() {}

// UnsafeMensajeroServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MensajeroServer will
// result in compilation errors.
type UnsafeMensajeroServer interface {
	mustEmbedUnimplementedMensajeroServer()
}

func RegisterMensajeroServer(s grpc.ServiceRegistrar, srv MensajeroServer) {
	s.RegisterService(&Mensajero_ServiceDesc, srv)
}

func _Mensajero_Conectar_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Registracion)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MensajeroServer).Conectar(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mensajero.Mensajero/Conectar",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MensajeroServer).Conectar(ctx, req.(*Registracion))
	}
	return interceptor(ctx, in, info, handler)
}

// Mensajero_ServiceDesc is the grpc.ServiceDesc for Mensajero service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Mensajero_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "mensajero.Mensajero",
	HandlerType: (*MensajeroServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Conectar",
			Handler:    _Mensajero_Conectar_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "pkg/mensajero.proto",
}
